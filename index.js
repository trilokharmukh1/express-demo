const express = require('express');
const app = express();
const path = require('path');
const port = process.env.PORT || 5000

// app.use(express.static('public'))
app.get('/', (req, res) => {
    res.sendFile('index.html', { root: path.join(__dirname, 'public') });
})


app.get('/users/:userId/books/:bookId', (req, res) => {
    res.send(req.params)
})


// app.listen(process.env.PORT || 3000);
app.listen(port, () => {
    console.log("port running");
})